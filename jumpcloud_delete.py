import json
import boto3
import os
import requests

import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Read 'jumpcloud-api-key' from AWS Systems Manager, Parameter store.
# This parameter will need to be added to the parameter store manually
# with the appropriate KMS key permissions
jumpcloud_api_key = boto3.client('ssm').get_parameter(
    Name='jumpcloud-api-key', WithDecryption=True)['Parameter']['Value']
headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'x-api-key': jumpcloud_api_key
}


def lambda_handler(event, context):

    # Getting Instance ID from event input
    instance_id = event['detail']['instance-id']

    # Creating Search payload
    search = {'filter': [{'amazonInstanceID': instance_id}],
              'fields': 'amazonInstanceID'}
    search = json.dumps(search)

    # Search URL
    url = 'https://console.jumpcloud.com/api/search/systems/'

    # Run search
    r = requests.post(url, headers=headers, data=search)

    # Review number of search results
    search_results = r.json()
    count = search_results['totalCount']

    # If one result proceed with deleting host from JumpCloud
    if count == 1:
        # Get JumpCloud System ID
        jumpcloud_system_id = search_results['results'][0]['_id']

        log = f'JumpCloud: Matching Instance Found. Deleting {instance_id} ({jumpcloud_system_id}) on JumpCloud.'
        logging.info(log)

        # Delete URL
        delete_url = 'https://console.jumpcloud.com/api/systems/' + jumpcloud_system_id

        # Run delete
        r = requests.delete(delete_url, headers=headers)

        # If delete returns 200, report success
        if r.status_code == 200:
            log = f'JumpCloud: {instance_id} ({jumpcloud_system_id}) deleted from JumpCloud.'
            logging.info(log)
            status = 200

        # If anything else returned report error
        else:
            log = f'JumpCloud: {instance_id} ({jumpcloud_system_id}) delete failed on JumpCloud.'
            logging.error(log)
            status = 400

    # If no results returned warn that instance was not found in JumpCloud
    elif count == 0:
        log = f'JumpCloud: {instance_id} not found on JumpCloud'
        logging.warn(log)
        status = 404

    # If else (more than one result), log an error with search results.
    else:
        log = 'JumpCloud: More than than one instance found on JumpCloud. Results logged.'
        logging.error(log)
        print(search_results)
        status = 409

    return {
        'statusCode': status,
        'body': log
    }
