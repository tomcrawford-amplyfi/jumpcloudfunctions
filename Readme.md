
# JumpCloud Functions

Functions for simplifying the JumpCloud operations within an AWS account

## Delete Terminated Instances
This function is an event drivven function that triggers on a CloudWatch "EC2 Instance State-change Notification". If this notification has a state of "terminated" the function will be invoked.

The function will then check if the terminated instance exists as a system on JumpCloud and if found it will proceed to delete the instance.

## Deploying the Functions

Functions are deployed to AWS Lambda using the [Serverless framework](https://www.serverless.com/framework/docs/providers/aws/guide/intro)

1. ```npm install```
2. ```npm run deploy```
